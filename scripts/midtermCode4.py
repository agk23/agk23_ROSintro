from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos #Importing constants
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt
    tau = 2.0 * pi
    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

def all_close(goal, actual, tolerance): 
    #all_close() checks to see if the target position 
    #and actual postition are withing a certain tolerance
 
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True
    #the function returns True or False depending on if
    #the actual position and the goal position are within
    #the tolerance

class MidtermProject(object):

    def __init__(self):
        super(MidtermProject, self).__init__()
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self):
        #the UR5e robot has 6 joints, so the position
        #of the arm can be described
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -1.57  #bottom rotation
        joint_goal[1] = -1.57  #first joint
        joint_goal[2] = -1.57 # second joint
        joint_goal[3] = 0 #end effector
        joint_goal[4] = 0 #end effector
        joint_goal[5] = 0 #end effector
        #this inital joint state came after a lot of testing, and led to 
        #very little singularities when drawing my initials
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_pose_goal(self, xgoal=.4 ,ygoal=.1 ,zgoal=.4):
        #go_to_pose_goal moves the end effector to a certain position withing the workspace
        #of the end effector. I modified the function to take in 3 arguments, the x,y and
        #z coordinates that I want the end effector to move to. Incase I don't provide any values,
        #I added default parameters.

        move_group = self.move_group
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1.0 #I did not see a pupose of including the orientation
                                      #as an argument, so I left it as 1.0.
        pose_goal.position.x = xgoal
        pose_goal.position.y = ygoal
        pose_goal.position.z = zgoal
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)


def main(): #The actual drawing of my initals in under main
            #which executes when I run the function
    try:
        test = MidtermProject()

        input(
            "Move to initial joint state:"
        )
        test.go_to_joint_state()
        
        test.go_to_pose_goal(.2, .35, .2) #'bottom left' of bounds
        input("Drawing A")
        test.go_to_pose_goal(.2, .35, .2) #moving to bottom left of A
        test.go_to_pose_goal(.2, .45, .48) 
        test.go_to_pose_goal(.2, .55, .2)
        test.go_to_pose_goal(.2, .5, .34)
        test.go_to_pose_goal(.2, .4, .34)
        
        input("Reseting")
        test.go_to_pose_goal(.2, .55, .50) #moving to top right of G
        input("Drawing G")
        test.go_to_pose_goal(.2, .35, .50)
        test.go_to_pose_goal(.2, .35, .2) 
        test.go_to_pose_goal(.2, .55, .2)
        test.go_to_pose_goal(.2, .55, .35)
        test.go_to_pose_goal(.2, .4, .35)
    
        input("Reseting")
        test.go_to_pose_goal(.2, .35, .2) #moving to bottom left of K
        input("Drawing K")
        test.go_to_pose_goal(.2, .35, .50)
        test.go_to_pose_goal(.2, .35, .35) 
        test.go_to_pose_goal(.2, .55, .50)
        test.go_to_pose_goal(.2, .35, .35)
        test.go_to_pose_goal(.2, .55, .2)

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()