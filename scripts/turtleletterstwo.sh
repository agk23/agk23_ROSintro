#!/usr/bin/bash

rosservice call /kill 'turtle1'

rosservice call /spawn 2 4 0 'turtle1'

rosservice call /spawn 7 6 0 'turtle2'


rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[2.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[2.0, -4.0, 0.0]' '[0.0, 0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-1.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call turtle2/set_pen 50 50 50 5 0

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[2.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[-2.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[2.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
